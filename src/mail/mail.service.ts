import { Inject, Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { CONFIG_OPTIONS } from '../common/common.constants';
import { MailModuleOptionsInterfaces } from './mail.interfaces';

@Injectable()
export class MailService {
  public transporter;
  constructor(
    @Inject(CONFIG_OPTIONS)
    private readonly options: MailModuleOptionsInterfaces,
  ) {
    // create reusable transporter object using the default SMTP transport
    this.transporter = nodemailer.createTransport({
      host: this.options.host,
      port: this.options.port,
      secure: false, // true for 465, false for other ports
      auth: {
        user: this.options.username,
        pass: this.options.password,
      },
    });
  }

  async sendEmail(from: string, to: string, subject: string, content: string) {
    try {
      await this.transporter.sendMail({
        from,
        to,
        subject: subject, // Subject line
        text: content,
        html: content, // html body
      });
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  sendVerificationEmail(email: string, code: string) {
    this.sendEmail(this.options.from, email, 'Verify Your Email', `code: ${code}`);
  }
}
