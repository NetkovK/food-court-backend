import { Test } from '@nestjs/testing';
import * as nodemailer from 'nodemailer';
import { MailService } from './mail.service';
import { CONFIG_OPTIONS } from '../common/common.constants';

const TEST_HOST = 'TEST_HOST';
const TEST_PORT = 'TEST_PORT';
const TEST_USERNAME = 'TEST_USERNAME';
const TEST_PASSWORD = 'TEST_PASSWORD';
const TEST_FROM = 'TEST_FROM';

jest.mock('nodemailer', () => ({
  createTransport: jest.fn(() => ({
    sendMail: jest.fn(),
  })),
}));

describe('MailService', () => {
  let service: MailService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        MailService,
        {
          provide: CONFIG_OPTIONS,
          useValue: {
            host: TEST_HOST,
            port: TEST_PORT,
            username: TEST_USERNAME,
            password: TEST_PASSWORD,
            from: TEST_FROM,
          },
        },
      ],
    }).compile();

    service = module.get<MailService>(MailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('sendVerificationEmail', () => {
    it('should call sendEmail', async () => {
      const sendVerificationEmailArgs = {
        email: 'some@email.com',
        code: 'some-code',
      };

      jest.spyOn(service, 'sendEmail').mockImplementation();
      await service.sendVerificationEmail(
        sendVerificationEmailArgs.email,
        sendVerificationEmailArgs.code,
      );
      expect(service.sendEmail).toHaveBeenCalledTimes(1);
      expect(service.sendEmail).toHaveBeenCalledWith(
        TEST_FROM,
        sendVerificationEmailArgs.email,
        expect.any(String),
        expect.any(String),
      );
    });
  });

  describe('sendEmail', () => {
    const sendEmailArgs = {
      from: '',
      to: '',
      subject: '',
      content: '',
    };

    it('should send email', async () => {
      const result = await service.sendEmail(
        sendEmailArgs.from,
        sendEmailArgs.to,
        sendEmailArgs.subject,
        sendEmailArgs.content,
      );
      expect(service.transporter.sendMail).toHaveBeenCalledTimes(1);
      expect(service.transporter.sendMail).toHaveBeenCalledWith(expect.any(Object));
      expect(result).toEqual(true);
    });
  });
});
