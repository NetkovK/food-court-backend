export interface MailModuleOptionsInterfaces {
  host: string;
  port: string;
  username: string;
  password: string;
  from: string;
}
