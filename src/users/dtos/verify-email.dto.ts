import { PickType, ObjectType, InputType } from '@nestjs/graphql';

import { CoreOutput } from '../../common/dtos/output.dto';
import { Verification } from '../entitys/verification.entity';

@InputType()
export class VerifyEmailInput extends PickType(Verification, ['code']) {}

@ObjectType()
export class VerifyEmailOutput extends CoreOutput {}
