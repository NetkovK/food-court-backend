import { SetMetadata } from '@nestjs/common';
import { UserRole } from '../users/entitys/user.entity';

export type AllowRoles = keyof typeof UserRole | 'Any';

export const Role = (roles: AllowRoles[]) => SetMetadata('roles', roles);
