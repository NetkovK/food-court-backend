import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entiteis/order.entity';
import { OrderResolver } from './order.resolver';
import { Restaurant } from '../restaurants/entities/restaurant.entity';
import { User } from '../users/entitys/user.entity';
import { OrderItem } from './entiteis/order-item.entity';
import { Dish } from '../restaurants/entities/dish.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Dish, Restaurant, User])],
  providers: [OrderService, OrderResolver],
})
export class OrdersModule {}
