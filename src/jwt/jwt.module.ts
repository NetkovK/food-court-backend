import { DynamicModule, Global, Module } from '@nestjs/common';
import { JwtService } from './jwt.service';
import { JwtModuleOptionsInterface } from './iwt.interfaces';
import { CONFIG_OPTIONS } from '../common/common.constants';

@Module({})
@Global()
export class JwtModule {
  static forRoot(options: JwtModuleOptionsInterface): DynamicModule {
    return {
      module: JwtModule,
      providers: [
        {
          provide: CONFIG_OPTIONS,
          useValue: options,
        },
        JwtService,
      ],
      exports: [JwtService],
    };
  }
}
