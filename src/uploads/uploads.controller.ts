import { Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as AWS from 'aws-sdk';
import { ConfigService } from '@nestjs/config';

const BUCKET_NAME = 'storageforphotosnietkovk';

interface IS3 {
  S3_KEY_ID: string;
  S3_ACCESS_KEY: string;
}

@Controller('uploads')
export class UploadsController {
  constructor(private configService: ConfigService<IS3>) {}

  @Post('')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file) {
    AWS.config.update({
      credentials: {
        accessKeyId: this.configService.get('S3_KEY_ID'),
        secretAccessKey: this.configService.get('S3_ACCESS_KEY'),
      },
    });

    try {
      // for creating Bucket
      // const upload = await new AWS.S3().createBucket({
      //   Bucket: BUCKET_NAME
      // }).promise()

      const originalName = Date.now() + file.originalname;
      const upload = await new AWS.S3()
        .putObject({
          Bucket: BUCKET_NAME,
          Body: file.buffer,
          Key: originalName,
          ACL: 'public-read',
        })
        .promise();

      const url = `https://${BUCKET_NAME}.s3.amazonaws.com/${originalName}`;
      return { url };
    } catch (e) {
      console.log('uploadFile error: ', e);
      return null;
    }
  }
}
